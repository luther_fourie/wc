package wc

import (
	"fmt"
)

var (
	App               = new(WC)
	terminateAppEvent = make(chan struct{})
)

type Interface interface {
	Get() *WC
	Init()
	Start()
	Stop()
}

type WC struct {
	children []Interface
}

func (wc *WC) Get() *WC {
	return wc
}

func (wc *WC) Init() {}

func (wc *WC) Start() {}

func (wc *WC) Stop() {}

func (wc *WC) Check(err error) {
	if err != nil {
		panic(err)
	}
}

func (wc *WC) Add(i Interface) {
	wc.children = append(wc.children, i)
	i.Init()
}

func Init(i Interface) {
	wc := i.Get()
	fmt.Println("Initializing...")

	if wc.children == nil {
		return
	}

	if len(wc.children) == 0 {
		return
	}

	for _, child := range wc.children {
		Init(child)
		child.Init()
	}
}

func Start(i Interface) {
	wc := i.Get()
	fmt.Println("Starting...")

	if wc.children == nil {
		return
	}

	if len(wc.children) == 0 {
		return
	}

	for _, child := range wc.children {
		Start(child)
		go child.Start()
	}
}

func Stop(i Interface) {
	wc := i.Get()
	fmt.Println("Stopping...")

	if wc.children == nil {
		return
	}

	if len(wc.children) == 0 {
		return
	}

	for _, child := range wc.children {
		Stop(child)
		go child.Stop()
	}
}

// "public" wrappers
func Add(i Interface) {
	App.Add(i)
}

func StartApp() {
	Start(App)
}

func StopApp() {
	Stop(App)
}

func WaitForTerminate() {
	<-terminateAppEvent
}

func Terminate() {
	terminateAppEvent <- struct{}{}
}

func Check(err error) {
	if err != nil {
		panic(err)
	}
}
